/** Basic async error wrapper so we don't need a trycatch in every function */
module.exports = function asyncWrapper(handler) {
  return function (request, response, next) {
    handler(request, response, next).catch((e) => {
      console.log(e);
      response.status(500).send({
        error: e.message
      });
    });
  };
}