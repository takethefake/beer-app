/** Package imports */
const express = require('express');
const router = express.Router({mergeParams: true});

/** File imports */
const asyncWrapper = require('./middleware/asyncWrapper');
const beerModel = require('../models/beer');

/** Routes */
router.get('/', getBeers);
router.get('/:id', getSingleBeer); 
router.delete('/:id', deleteBeer);
router.put('/:id', updateBeer);
router.post('/', asyncWrapper(createBeer));

/**
 * Get all beers
 * @param {*} req
 * @param {*} res
 */
async function getBeers(req, res) {
  try {
    const beers = await beerModel.find();
    res.send({
      status: 'OK',
      data: beers
    });
  } catch (e) {
    res.status(500).send(e);
  }
}

/**
 * Get specific beer
 * @param {*} req
 * @param {*} res
 */
async function getSingleBeer (req, res) {
  try {
    const beer = await beerModel.findById(req.params.id);
    res.send({
      status: 'OK',
      data: beer
    });
  } catch (e) {
    res.status(500).send(e);
  }
}

/**
 * Create a new beer
 * @param {*} req
 * @param {*} res
 */
async function createBeer (req, res) {
  const newBeer = new beerModel(req.body);
  const createdBeer = await newBeer.save();
  res.status(201).send({
    status: 'CREATED',
    data: createdBeer
  });
}

/**
 * Delete a specific beer
 * @param {*} req
 * @param {*} res
 */
async function deleteBeer (req, res) {
  try {
    await beerModel.findByIdAndRemove(req.params.id);
    res.status(204).send();
  } catch (e) {
    res.status(500).send(e);
  }
}

/**
 * Update a beer
 * @param {*} req
 * @param {*} res
 */
async function updateBeer (req, res) {
  try {
    const beerId = req.params.id;
    const beer = await beerModel.findOneAndUpdate({
      _id: beerId
    }, {
      $set: req.body
    }, {
      new: true
    }); 
    
    res.send({
      status: 'OK',
      data: beer
    })
  } catch (e) {
    res.status(500).send(e);
  }
}

/** Exports */
module.exports = {
  router
}