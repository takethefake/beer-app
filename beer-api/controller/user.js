/** Package imports */
const express = require('express');
const router = express.Router({
  mergeParams: true
});
const request = require('request-promise');

/** Routes */
router.get('/', getUsers);

/**
 * Get users from external api
 * @param {*} req 
 * @param {*} res 
 */
async function getUsers(req, res) {
  try {
    const requestOptions = {
      method: 'GET',
      uri: 'https://jsonplaceholder.typicode.com/users',
      json: true
    };

    const users = await request(requestOptions);
    const changedUsers = [];

    for (const user of users) {
      const newUser = {
        name: user.name,
        email: user.email
      }
      changedUsers.push(newUser);
    }
    res.send(changedUsers);
  } catch (e) {
    res.status(500).send(e);
  }
}

/** Exports */
module.exports = {
  router
};