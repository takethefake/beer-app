/** Package imports */
const express = require('express');
const router = express.Router();

/** File imports */
const beer = require('./beer');
const user = require('./user');
const logTime = require('./middleware/timedLog');

/** Routes */
router.use('/beer', logTime, beer.router);
router.use('/user', user.router);

/** Exports */
module.exports = { router };
