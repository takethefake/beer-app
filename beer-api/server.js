/** Package imports */
const bodyParser = require('body-parser');
const config = require('config');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

/** File import */
const router = require('./controller/router');

/** Connect to mongoose */
mongoose.connect(
  config.db,
  {
    useNewUrlParser: true,
  }
);

/** Initialize express */
const app = express();

/** Middleware */
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.text());

/** Routes */
app.get('/', (req, res) => {
  res.send('Hello World');
});
app.use('/api', router.router);

console.log('Server is listening on', config.port);

app.listen(config.port);

/** Exports */
module.exports = app;
