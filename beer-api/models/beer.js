/** Package imports */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

/** Setup Schema */
const beerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  tasteRating: {
    type: Number,
    required: true,
    default: 0
  },
  alcPercent: {
    type: Number,
    required: true,
    default: 0
  },
});

/** Exports */
module.exports = mongoose.model('Beer', beerSchema);